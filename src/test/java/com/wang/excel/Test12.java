package com.wang.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import org.junit.Test;

import com.wang.excel.excelutils.ExportExcelUtil;
import com.wang.excel.excelutils.UploadExcelUtil;
import com.wang.excel.utlis.Member;
import com.wang.excel.utlis.ProjectExcelBO;

public class Test12 {
	public static void main(String[] args) {

		// 测试学生
		ExportExcelUtil<ProjectExcelBO> ex = new ExportExcelUtil<ProjectExcelBO>();
		String[] headers = { "项目名称", "主办方(学会或辉瑞)", "执行方", "申请人", "状态", "ACM编号",
				"审批日期（acm时间）" + "EPAY编号1", "首付金额", "审批日期1", "EPAY编号2", "二付金额 ",
				"审批日期2", "EPAY编号3", "三付", "审批日期3", "EPAY编号4", "四付", "审批日期4",
				"1st date" + "是否提交", "2nd date", "是否提交", "3rd date", "是否提交",
				"4th date", "是否提交", "5th date", "是否提交 ", "是否已结算", "是否已退款" };
		List<ProjectExcelBO> dataset = new ArrayList<ProjectExcelBO>();

		dataset.add(new ProjectExcelBO("", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", ""));
		dataset.add(new ProjectExcelBO("", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", ""));
		// dataset.add(new Student(20000002, "", 24, false, new Date(),123));
		// dataset.add(new Student(30000003, "王五", 22, true, new Date()));
		// dataset.add(new Student(30000003, "张飞", 25, true, new Date()));
		// dataset.add(new Student(30000003, "关羽", 23, true, new Date()));
		OutputStream out;
		try {
			out = new FileOutputStream("D:/" + Math.random() + "a.xls");
			//ex.readExcel("测试POI导出EXCEL文档", headers, dataset, out, "yyyy-MM-dd");
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, "导出成功!");
			System.out.println("excel导出成功！");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void api_exportExcel() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sdf.parse("2018-09-2");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int num = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

		calendar.setTime(date);
		// 第几周
		int week = calendar.get(Calendar.WEEK_OF_MONTH);
		System.out.println(week);
		System.out.println(num);

	}
	
	
	@Test
	public void uploadExcel(){
		UploadExcelUtil upload = new UploadExcelUtil();
		File file = new File("C:\\Users\\dell\\Desktop\\工作簿11.xls");
		List<Member> memberList = upload.extract(file);
		for(Member num: memberList){
			System.out.println(num.toString());
		}
	}

}
