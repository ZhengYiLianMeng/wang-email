package com.wang.excel.excelutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.wang.excel.utlis.Member;

public class UploadExcelUtil{

	
	public List<Member> extract(File file_name){
        //List<Member> members = new ArrayList<>();
        List<Member> members = new ArrayList<>();
        try {
            InputStream is = new FileInputStream(file_name);
            HSSFWorkbook xssfWorkbook = new HSSFWorkbook(is);
            //只读取第一个sheet进行处理
            HSSFSheet sheet = xssfWorkbook.getSheetAt(0);

            //处理当前sheet，循环读取每一行
            for(int row_num = 1; row_num < (sheet.getLastRowNum()+1);row_num++){
                //System.out.println(sheet.getLastRowNum());
            	HSSFRow xss_row = sheet.getRow(row_num);
                Member member = new Member();
                HSSFCell call = xss_row.getCell(0);
                member.setName(getCellValue(xss_row.getCell(0)));
                member.setPhone(getCellValue(xss_row.getCell(1)));
                member.setCard_name(getCellValue(xss_row.getCell(2)));
                member.setLoose_change(getCellValue(xss_row.getCell(3)));
                members.add(member);
            }
            //}
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return members;
    }
	
	
	public static String getCellValue(Cell cell) {
		String value = null;
		DecimalFormat df = new DecimalFormat("0"); // 格式化number String字符
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd"); // 日期格式化
		DecimalFormat df2 = new DecimalFormat("0.00"); // 格式化数字
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			value = cell.getRichStringCellValue().getString();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			if ("General".equals(cell.getCellStyle().getDataFormatString())) {
				value = df.format(cell.getNumericCellValue());
			} else if ("m/d/yy".equals(cell.getCellStyle().getDataFormatString())) {
				value = sdf.format(cell.getDateCellValue());
			} else {
				value = df2.format(cell.getNumericCellValue());
			}
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			value = cell.getBooleanCellValue()+"";
			break;
		case Cell.CELL_TYPE_BLANK:
			value = "";
			break;
		default:
			break;
		}
		return value;
	}

	

}
