package com.wang.excel.excelutils;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * 基于POI导出excel工具
 * 
 * @author wangfuwei
 * @date 2018年8月11日
 */
public class ExportExcelUtil<T> {

	/*
	 * private static HSSFCellStyle getTitleStyle2(HSSFWorkbook workbook,
	 * HSSFCellStyle titleStyle) { // 设置表头样式
	 * titleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	 * titleStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	 * titleStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	 * titleStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
	 * titleStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
	 * titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	 * titleStyle.setFillBackgroundColor(HSSFColor.LIGHT_GREEN.index);
	 * titleStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index); // 生成表头字体
	 * HSSFFont font = workbook.createFont(); font.setFontHeightInPoints((short)
	 * 17); font.setFontName("宋体"); font.setBoldweight(HSSFFont.SS_NONE); //
	 * 把字体应用到当前的样式 titleStyle.setFont(font); return titleStyle; }
	 */

	/**
	 * 生成表头样式
	 * 
	 * @param workbook
	 * @return
	 * @return HSSFCellStyle
	 * @author: wangfuwei
	 * @date:2018年8月11日
	 */
	private static HSSFCellStyle getTitleStyle(HSSFWorkbook workbook,
			HSSFCellStyle titleStyle) {
		// 设置表头样式
		titleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		titleStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		titleStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		titleStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		titleStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		titleStyle.setFillBackgroundColor(HSSFColor.LIGHT_GREEN.index);
		titleStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
		// 生成表头字体
		HSSFFont font = workbook.createFont();
		font.setFontHeightInPoints((short) 20);
		font.setFontName("宋体");
		font.setBoldweight(HSSFFont.SS_NONE);
		// 把字体应用到当前的样式
		titleStyle.setFont(font);
		return titleStyle;
	}

	/**
	 * 生成具体内容样式
	 * 
	 * @param workbook
	 * @return
	 * @return HSSFCellStyle
	 * @author: wangfuwei
	 * @date:2018年8月11日
	 */
	private static HSSFCellStyle getContentStyle(HSSFWorkbook workbook,
			HSSFCellStyle contentStyle) {
		// 设置内容样式
		contentStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		contentStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		contentStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		contentStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		contentStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		contentStyle.setFillBackgroundColor(HSSFColor.LIGHT_TURQUOISE.index);
		contentStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
		// 设置内容字体
		HSSFFont fontSec = workbook.createFont();
		fontSec.setFontName("宋体");
		fontSec.setFontHeightInPoints((short) 15);
		fontSec.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		contentStyle.setFont(fontSec);
		return contentStyle;
	}

	/**
	 * 生成表格表头
	 * 
	 * @param sheet表格
	 * @param headers表头
	 * @param style表头格式
	 * @return void
	 * @author: wangfuwei
	 * @date:2018年8月11日
	 */
	private static void setHeaders(HSSFRow row, HSSFSheet sheet,
			String[] headers, HSSFCellStyle style) {
		// 产生表格标题行
		for (short i = 0; i < headers.length; i++) {
			String value = headers[i];
			HSSFCell cell = row.createCell(i);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(value);
			cell.setCellValue(text);
		}
	}

	/**
	 * 填充单元格行
	 * 
	 * @param workbook
	 * @param dataset
	 * @param row
	 * @param sheet
	 * @param style2
	 * @param out
	 * @param pattern
	 * @param patriarch
	 * @return void
	 * @author: wangfuwei
	 * @date:2018年8月11日
	 */
	@SuppressWarnings("unchecked")
	private void getContens(HSSFWorkbook workbook, Collection<T> dataset,
			HSSFRow row, HSSFSheet sheet, HSSFCellStyle style2,
			OutputStream out, String pattern, HSSFPatriarch patriarch) {
		// 遍历集合数据，产生数据行
		Iterator<T> it = dataset.iterator();
		int index = 0;
		while (it.hasNext()) {
			index++;
			row = sheet.createRow(index);
			T t = (T) it.next();
			// 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
			Field[] fields = t.getClass().getDeclaredFields();
			for (short i = 0; i < fields.length; i++) {
				HSSFCell cell = row.createCell(i);
				cell.setCellStyle(style2);
				//row.setHeightInPoints(100);//设置行的高度是100个点     设置单元格高度
				
				
				Field field = fields[i];
				String fieldName = field.getName();
				String getMethodName = "get"
						+ fieldName.substring(0, 1).toUpperCase()
						+ fieldName.substring(1);
				try {
					Class tCls = t.getClass();
					Method getMethod = tCls.getMethod(getMethodName,
							new Class[] {});
					Object value = getMethod.invoke(t, new Object[] {});
					// 判断值的类型后进行强制类型转换
					String textValue = null;
					if (value instanceof Boolean) {
						boolean bValue = (Boolean) value;
						textValue = "男";
						if (!bValue) {
							textValue = "女";
						}
					} else if (value instanceof Date) {
						Date date = (Date) value;
						SimpleDateFormat sdf = new SimpleDateFormat(pattern);
						textValue = sdf.format(date);
					} else if (value instanceof byte[]) {
						// 有图片时，设置行高为60px;
						row.setHeightInPoints(60);
						// 设置图片所在列宽度为80px,注意这里单位的一个换算
						sheet.setColumnWidth(i, (short) (35.7 * 80));
						// sheet.autoSizeColumn(i);
						byte[] bsValue = (byte[]) value;
						HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0,
								1023, 255, (short) 6, index, (short) 6, index);

						anchor.setAnchorType(2);
						patriarch.createPicture(anchor, workbook.addPicture(
								bsValue, HSSFWorkbook.PICTURE_TYPE_JPEG));
					} else {
						// 其它数据类型都当作字符串简单处理
						textValue = value.toString();
					}
					// 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成
					if (textValue != null) {
						Pattern p = Pattern.compile("^//d+(//.//d+)?$");
						Matcher matcher = p.matcher(textValue);
						if (matcher.matches()) {
							// 是数字当作double处理
							cell.setCellValue(Double.parseDouble(textValue));
						} else {
							HSSFRichTextString richString = new HSSFRichTextString(
									textValue);
							HSSFFont font3 = workbook.createFont();
							font3.setColor(HSSFColor.BLUE.index);
							richString.applyFont(font3);
							cell.setCellValue(richString);
						}
					}
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} finally {
					// 清理资源
				}
			}
		}
		try {
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 生成Excel
	 * 
	 * @param title
	 * @param headers表头内容
	 * @param dataset表格内容
	 * @param out输入流
	 * @param pattern时间格式
	 * @return void
	 * @author: wangfuwei
	 * @date:2018年8月11日
	 */
	public void readExcel(String title, String[] headers,
			Collection<T> dataset, OutputStream out, String pattern) {

		// 声明一个工作薄
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 生成一个表格
		HSSFSheet sheet = workbook.createSheet(title);

		HSSFRow rowOther = sheet.createRow(0);
		HSSFCell cellOther = rowOther.createCell(4);
		// cellOther.setCellValue("多行多列合并");
		//sheet.addMergedRegion(new CellRangeAddress(0, 2, 4, 7));

		// 设置表格默认列宽度为15个字节
		sheet.setDefaultColumnWidth((short) 30);
		// 生成表头样式
		HSSFCellStyle titleStyle = workbook.createCellStyle();
		titleStyle = getTitleStyle(workbook, titleStyle);
		// HSSFCellStyle titleStyle2 = getTitleStyle2(workbook, titleStyle);
		// 生成具体内容样式
		HSSFCellStyle contentStyle = workbook.createCellStyle();
		contentStyle = getContentStyle(workbook, contentStyle);

		// 声明一个画图的顶级管理器
		HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
		// 定义注释的大小和位置,详见文档
		HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,
				0, 0, 0, (short) 4, 2, (short) 6, 5));
		// 设置注释内容
		comment.setString(new HSSFRichTextString("可以在POI中添加注释！"));
		// 设置注释作者，当鼠标移动到单元格上是可以在状态栏中看到该内容.
		comment.setAuthor("leno");

		// 生成表格表头
		HSSFRow row = sheet.createRow(0);
		
		//表头行高
		//row.setZeroHeight(false);
		//row.setHeight((short) 1000);
		setHeaders(row, sheet, headers, titleStyle);
		// setHeaders(row, sheet, headers, titleStyle2);
		getContens(workbook, dataset, row, sheet, contentStyle, out, pattern,
				patriarch);
	}

}
