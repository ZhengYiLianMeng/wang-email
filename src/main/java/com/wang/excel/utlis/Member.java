package com.wang.excel.utlis;

public class Member {
	private String name;
    private String phone;
    private String card_name;
    private String loose_change;
	public Member(String name, String phone, String card_name,
			String loose_change) {
		super();
		this.name = name;
		this.phone = phone;
		this.card_name = card_name;
		this.loose_change = loose_change;
	}
	public Member() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCard_name() {
		return card_name;
	}
	public void setCard_name(String card_name) {
		this.card_name = card_name;
	}
	public String getLoose_change() {
		return loose_change;
	}
	public void setLoose_change(String loose_change) {
		this.loose_change = loose_change;
	}
	
	@Override
	public String toString() {
		return "Member [name=" + name + ", phone=" + phone + ", card_name="
				+ card_name + ", loose_change=" + loose_change + "]";
	}
    
}
