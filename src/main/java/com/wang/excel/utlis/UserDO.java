package com.wang.excel.utlis;

import java.io.Serializable;
import java.util.Date;

public class UserDO implements Serializable {

	private static final long serialVersionUID = 1L;
	private boolean sex;
	private Date birthday;

	public boolean isSex() {
		return sex;
	}

	public void setSex(boolean sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

}
