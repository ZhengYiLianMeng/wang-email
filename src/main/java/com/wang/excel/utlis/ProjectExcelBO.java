package com.wang.excel.utlis;

public class ProjectExcelBO {

	private String projectname; // 项目名称
	private String sponsor; // 主办方
	private String executorName; // 执行人ID
	private String applicantName; // 申请人ID
	private String state; // 状态
	private String acmnumber; // ACM编号
	private String one;
	private String two;
	private String three;
	private String four;
	private String five;
	private String six;
	private String seven;
	private String eight;
	private String nine;
	private String ten;
	private String eleven;
	private String twelve;
	private String thirteen;
	private String fourteen;
	private String fifteen;
	private String sixteen;
	private String seventeen;
	private String nineteen;
	private String twenty;
	private String twentyOne;
	private String twentyTwo;
	private String twentyThree;
	private String twentyFour;
	private String twentyFive;
	private String twentySix;

	public ProjectExcelBO() {

	}

	public ProjectExcelBO(String projectname, String sponsor,
			String executorName, String applicantName, String state,
			String acmnumber, String one, String two, String three,
			String four, String five, String six, String seven, String eight,
			String nine, String ten, String eleven, String twelve,
			String thirteen, String fourteen, String fifteen, String sixteen,
			String seventeen, String nineteen, String twenty, String twentyOne,
			String twentyTwo, String twentyThree, String twentyFour,
			String twentyFive, String twentySix) {
		super();
		this.projectname = projectname;
		this.sponsor = sponsor;
		this.executorName = executorName;
		this.applicantName = applicantName;
		this.state = state;
		this.acmnumber = acmnumber;
		this.one = one;
		this.two = two;
		this.three = three;
		this.four = four;
		this.five = five;
		this.six = six;
		this.seven = seven;
		this.eight = eight;
		this.nine = nine;
		this.ten = ten;
		this.eleven = eleven;
		this.twelve = twelve;
		this.thirteen = thirteen;
		this.fourteen = fourteen;
		this.fifteen = fifteen;
		this.sixteen = sixteen;
		this.seventeen = seventeen;
		this.nineteen = nineteen;
		this.twenty = twenty;
		this.twentyOne = twentyOne;
		this.twentyTwo = twentyTwo;
		this.twentyThree = twentyThree;
		this.twentyFour = twentyFour;
		this.twentyFive = twentyFive;
		this.twentySix = twentySix;
	}

	public String getTwentyOne() {
		return twentyOne;
	}

	public void setTwentyOne(String twentyOne) {
		this.twentyOne = twentyOne;
	}

	public String getTwentyTwo() {
		return twentyTwo;
	}

	public void setTwentyTwo(String twentyTwo) {
		this.twentyTwo = twentyTwo;
	}

	public String getTwentyThree() {
		return twentyThree;
	}

	public void setTwentyThree(String twentyThree) {
		this.twentyThree = twentyThree;
	}

	public String getTwentyFour() {
		return twentyFour;
	}

	public void setTwentyFour(String twentyFour) {
		this.twentyFour = twentyFour;
	}

	public String getTwentyFive() {
		return twentyFive;
	}

	public void setTwentyFive(String twentyFive) {
		this.twentyFive = twentyFive;
	}

	public String getTwentySix() {
		return twentySix;
	}

	public void setTwentySix(String twentySix) {
		this.twentySix = twentySix;
	}

	public String getExecutorName() {
		return executorName;
	}

	public void setExecutorName(String executorName) {
		this.executorName = executorName;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getAcmnumber() {
		return acmnumber;
	}

	public void setAcmnumber(String acmnumber) {
		this.acmnumber = acmnumber;
	}

	public String getOne() {
		return one;
	}

	public void setOne(String one) {
		this.one = one;
	}

	public String getTwo() {
		return two;
	}

	public void setTwo(String two) {
		this.two = two;
	}

	public String getThree() {
		return three;
	}

	public void setThree(String three) {
		this.three = three;
	}

	public String getFour() {
		return four;
	}

	public void setFour(String four) {
		this.four = four;
	}

	public String getFive() {
		return five;
	}

	public void setFive(String five) {
		this.five = five;
	}

	public String getSix() {
		return six;
	}

	public void setSix(String six) {
		this.six = six;
	}

	public String getSeven() {
		return seven;
	}

	public void setSeven(String seven) {
		this.seven = seven;
	}

	public String getEight() {
		return eight;
	}

	public void setEight(String eight) {
		this.eight = eight;
	}

	public String getNine() {
		return nine;
	}

	public void setNine(String nine) {
		this.nine = nine;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getEleven() {
		return eleven;
	}

	public void setEleven(String eleven) {
		this.eleven = eleven;
	}

	public String getTwelve() {
		return twelve;
	}

	public void setTwelve(String twelve) {
		this.twelve = twelve;
	}

	public String getThirteen() {
		return thirteen;
	}

	public void setThirteen(String thirteen) {
		this.thirteen = thirteen;
	}

	public String getFourteen() {
		return fourteen;
	}

	public void setFourteen(String fourteen) {
		this.fourteen = fourteen;
	}

	public String getFifteen() {
		return fifteen;
	}

	public void setFifteen(String fifteen) {
		this.fifteen = fifteen;
	}

	public String getSixteen() {
		return sixteen;
	}

	public void setSixteen(String sixteen) {
		this.sixteen = sixteen;
	}

	public String getSeventeen() {
		return seventeen;
	}

	public void setSeventeen(String seventeen) {
		this.seventeen = seventeen;
	}

	public String getNineteen() {
		return nineteen;
	}

	public void setNineteen(String nineteen) {
		this.nineteen = nineteen;
	}

	public String getTwenty() {
		return twenty;
	}

	public void setTwenty(String twenty) {
		this.twenty = twenty;
	}

	@Override
	public String toString() {
		return "ProjectExcelBO [projectname=" + projectname + ", sponsor="
				+ sponsor + ", executorName=" + executorName
				+ ", applicantName=" + applicantName + ", state=" + state
				+ ", acmnumber=" + acmnumber + ", one=" + one + ", two=" + two
				+ ", three=" + three + ", four=" + four + ", five=" + five
				+ ", six=" + six + ", seven=" + seven + ", eight=" + eight
				+ ", nine=" + nine + ", ten=" + ten + ", eleven=" + eleven
				+ ", twelve=" + twelve + ", thirteen=" + thirteen
				+ ", fourteen=" + fourteen + ", fifteen=" + fifteen
				+ ", sixteen=" + sixteen + ", seventeen=" + seventeen
				+ ", nineteen=" + nineteen + ", twenty=" + twenty + "]";
	}

}
